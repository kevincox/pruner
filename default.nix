{
	nixpkgs ? import <nixpkgs> {},
	naersk ? nixpkgs.pkgs.callPackage (fetchTarball "https://github.com/nmattia/naersk/archive/master.tar.gz") {},
}:
with nixpkgs;
rec {
	pruner = naersk.buildPackage {
		root = pkgs.nix-gitignore.gitignoreSource [".*" "*.nix"] ./.;
	};
}
